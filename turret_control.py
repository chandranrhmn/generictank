from pyvx import *
from VxSim import *
import math

DEG2GRAD = math.pi/180
RAD2DEG  = 1./DEG2GRAD

def on_add_to_owner(self, owner):
   self.chassis_turret = self.getInput('chassis_turret').toConstraint()
   self.turret_cannon = self.getInput('turret_cannon').toConstraint()
   self.azimuth_deg = self.getOutput('azimuth_deg')
   self.elevation_deg = self.getOutput('elevation_deg')


# -------------------------

def pre_step(self):
   azimuthRot           = self.getInput('turret_azimuth').toReal()*DEG2GRAD
   elevationRot         = self.getInput('cannon_elevation').toReal()*DEG2GRAD

   self.chassis_turret.coordinates[0].motorDesiredVelocity = azimuthRot
   self.turret_cannon.coordinates[0].motorDesiredVelocity = elevationRot



def post_step(self):
   current_azimuth      = self.chassis_turret.coordinates[0].currentStatePosition * RAD2DEG 
   current_elevation    = self.turret_cannon.coordinates[0].currentStatePosition * RAD2DEG
  
   if current_azimuth >= 0:
      current_azimuth      = current_azimuth % 360.
   else:
      current_azimuth = 360 - abs(current_azimuth) % 360.
    

   self.azimuth_deg.setValue(current_azimuth)
   self.elevation_deg.setValue(current_elevation)

   #azimuthPos = self.chassis_turret.coordinates['Angular'].currentStatePosition * RAD2DEG;

   #tes tes tes
   #hello world

   #trying more
